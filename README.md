# Guided Jupyter-Tour: Optimal Transport

#### Private CRC 1456 Binderhub:
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=voila)

#### Public CRC 1456 Binderhub:
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fguided-jupyter-tour-optimal-transport/HEAD?urlpath=voila)

#### Other LiveDocs:
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/guided-jupyter-tour-optimal-transport/lab/index.html)
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/guided-jupyter-tour-optimal-transport/container_registry)

#### Static HTMLs (with limited functionality):
[![static-html](https://img.shields.io/badge/HTML-Intro.ipynb-white)](https://crc1456.pages.gwdg.de/livedocs/guided-jupyter-tour-optimal-transport/files/Intro.html)
[![static-html](https://img.shields.io/badge/HTML-Colocalization_with_Optimal_Transport-white)](https://crc1456.pages.gwdg.de/livedocs/guided-jupyter-tour-optimal-transport/files/Colocalization_with_Optimal_Transport.html)
[![static-html](https://img.shields.io/badge/HTML-Optimal_Transport_and_Xray_Tomography-white)](https://crc1456.pages.gwdg.de/livedocs/guided-jupyter-tour-optimal-transport/files/Optimal_Transport_and_X-ray_Tomography.html)
[![static-html](https://img.shields.io/badge/HTML-Pointwise_Correlation-white)](https://crc1456.pages.gwdg.de/livedocs/guided-jupyter-tour-optimal-transport/files/Pointwise_Correlation.html)

**************

## Overview

In this collection of 4 Jupyter notebooks, the reader is gently and informally introduced to selected topics of Optimal Transport and Particle Colocalization. They can be accessed through here:

<ol>
    <li>
        <a href="Intro.ipynb"><b>Introduction: But what is Optimal Transport?</b><br><img src='assets/Dirt.png' width='431px' /></a>
    </li>
    <li>
        <a href="Pointwise_Correlation.ipynb"><b>Pixel-based Particle Colocalization</b><br><img src='assets/Pixels.png' width='431px' /></a>
    </li>
    <li>
        <a href="Colocalization_with_Optimal_Transport.ipynb"><b>Connecting the Dots with Optimal Transport - Particle Colocalization</b><br><img src='assets/Particles.png' width='431px' /></a>
    </li>
    <li>
        <a href="Optimal_Transport_and_X-ray_Tomography.ipynb"><b>The Patient Space - Spotting Alzheimer's Disease with optimal Transport</b><br><img src='assets/Cells.jpg' width='431px' /></a>
    </li>
</ol>


The work of the University of Göttingen's [Collaborative Research Center 1456](https://www.uni-goettingen.de/en/628179.html) is especially in focus and provides the context from which to understand and motivate the material. Readers are encouraged to play with and modify the code for themselves.

**Audience -** The notebooks were written for intermediate Bachelor's students in mathematics or Computer Science. Especially notebooks 3 and 4 treat recent research and may be interesting for Master's students and above, too. Audiences without mathematical background are welcome to skip more technical explanations and focus on the introductions, interactive widgets and graphs. These are purpusefully made accessible without much in the way of mathematical background knowledge.

**Methods -** The notebooks are written in Python 3 with Jupyter and `ipywidgets` as the interactive elements. They utilize `numpy` and `scipy`. A library for solving optimal transport problems developed by Prof. Bernhard Schmitzer was also used, which is open source and can be found [here](https://github.com/bernhard-schmitzer/UnbalancedLOT).

**Project Duration -** The itself project was put together from May to July 2022. It builds on previous work in the form of Bachelor's theses by Fabian Fieberg and Thilo Stier.

**Authorship -** Programming: Thilo Stier, Fabian Fieberg and Lennart Finke. Text: Lennart Finke. Supervised by Prof. Dr. Cristoph Lehrenfeld & Prof. Dr. Bernhard Schmitzer. Images

**License -** the notebooks themselves are licenced under MIT. For the images' license, see the note below. The library mentioned above is also included here, under `lib/` and `data/`. Licensed under [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause), © 2021 Bernhard Schmitzer.

****

[1] From the study *Three-dimensional virtual histology of the human hippocampus based on phase-contrast computed tomography* published in *Proceedings of the National Academy of Sciences* Vol. 118 under the [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license, © 2021 the Authors. Special thanks to Prof. Dr. Tim Salditt, by whom permission of use was kindly given.
